# -*- coding: utf-8 -*-
"""
Created on Thu Mar 29 09:37:36 2018

@author: a010943
"""

import tensorflow as tf
import numpy as np
from preTrainedFC import preTrainedFC as ptfc
from real_value_RBM_tf import RBM
import pickle


def load():
    with open("../data/mnist.pkl",'rb') as f:
        mnist = pickle.load(f)
    return mnist["training_images"], mnist["training_labels"], mnist["test_images"], mnist["test_labels"]

inputs_placeholder = tf.placeholder(np.float32,[None,28*28])
CD_order = tf.Variable(1)
learning_rate = tf.Variable(0.005)
momentum = tf.Variable(0.9)

training, labels, test, test_labels = load()
shuffling = np.random.permutation(training.shape[0])
training = training[shuffling,:]
labels = labels[shuffling]
indexBatch = 0
def getBatch(size):
    global indexBatch
    global training
    global labels
    batch = np.float32(training[indexBatch:indexBatch+size])/256
    targets = labels[indexBatch:indexBatch+size]
    indexBatch += size
    if(indexBatch+size>training.shape[0]):
        indexBatch = 0
        shuffling = np.random.permutation(training.shape[0])
        training = training[shuffling,:]
        labels = labels[shuffling]
    return batch,targets
    
layer1 = RBM(32,training.shape[1])
layer2 = RBM(16,32)
layer3 = RBM(4,16)

sess = tf.Session()

sess.run(tf.global_variables_initializer())

optimizer = tf.train.GradientDescentOptimizer(learning_rate)
tf_train_step = optimizer.apply_gradients(grads_and_vars=\
layer1.train_step_grads_vars(inputs_placeholder,learning_rate,momentum,CD_order))
for i in range(5000):
    batch, targets = getBatch(500)
    feed_dict = {inputs_placeholder : batch,
                 learning_rate : 0.005,
                 momentum : 0.8,
                 CD_order : 1}
    sess.run(tf_train_step,feed_dict=feed_dict)
    if i%100 == 0:
        print('iteration %d' % i)

sess2 = tf.Session()
hiddenBatch = layer1.getHidden(inputs_placeholder,CD_order.eval(session=sess))
optimizer = tf.train.GradientDescentOptimizer(learning_rate)
tf_train_step2 = optimizer.apply_gradients(grads_and_vars=\
layer2.train_step_grads_vars(hiddenBatch,learning_rate,momentum,CD_order))
sess2.run(tf.global_variables_initializer())
for i in range(5000):
    batch, targets = getBatch(100)
    feed_dict = {inputs_placeholder : batch,
             learning_rate : 0.005,
             momentum : 0.8,
             CD_order : 1}
    sess2.run(tf_train_step2,feed_dict=feed_dict)
    if i%100 == 0:
        print('iteration %d' % i)
        
sess3 = tf.Session()
hiddenBatch1 = layer2.getHidden(inputs_placeholder,CD_order.eval(session=sess))
hiddenBatch2 = layer2.getHidden(hiddenBatch1,CD_order.eval(session=sess))
optimizer = tf.train.GradientDescentOptimizer(learning_rate)
tf_train_step3 = optimizer.apply_gradients(grads_and_vars=\
layer3.train_step_grads_vars(hiddenBatch2,learning_rate,momentum,CD_order))
sess3.run(tf.global_variables_initializer())
for i in range(5000):
    batch, targets = getBatch(100)
    feed_dict = {inputs_placeholder : batch,
             learning_rate : 0.005,
             momentum : 0.8,
             CD_order : 1}
    sess3.run(tf_train_step2,feed_dict=feed_dict)
    if i%100 == 0:
        print('iteration %d' % i)


W1 = layer1.mW.eval(session=sess)
bj1 = layer1.mBj.eval(session=sess)
W2 = layer2.mW.eval(session=sess2)
bj2 = layer2.mBj.eval(session=sess2)
W3 = layer3.mW.eval(session=sess3)
bj3 = layer3.mBj.eval(session=sess3)

myFC = ptfc()
myFC.addPreTrainedLayer(np.transpose(W1),bj1)
myFC.addPreTrainedLayer(np.transpose(W2),bj2)
myFC.addPreTrainedLayer(np.transpose(W3),bj3)
myFC.addPreTrainedLayer(W3,-bj2)
myFC.addPreTrainedLayer(W2,-bj1)
myFC.addPreTrainedLayer(W1,np.zeros(28*28))

sess4 = tf.Session()
outputs = myFC.compute(inputs_placeholder)
lossFC = tf.reduce_mean(tf.squared_difference(inputs_placeholder,outputs))
optimizerFT = tf.train.AdamOptimizer(0.0001)
fineTune = optimizerFT.minimize(lossFC)
sess4.run(tf.global_variables_initializer())
for i in range(15000):
    batch, targets = getBatch(100)
    feed_dict = {inputs_placeholder : batch}
    sess4.run(fineTune,feed_dict=feed_dict)
    if i%100==0:
        print('iter %d' % i)
        lossFC_val = sess2.run(lossFC,feed_dict=feed_dict)
        print('loss %f' % lossFC_val)
#    
myFC.save(sess2)
