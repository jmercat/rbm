# -*- coding: utf-8 -*-
"""
Created on Fri Mar 23 15:40:42 2018

@author: a010943
"""
import tensorflow as tf
import numpy as np

class preTrainedLayer:
    def __init__(self,weights,biases):
        self.mW = tf.Variable(weights,dtype='float32',name="preTrained/W")
        self.mB = tf.Variable(biases,dtype='float32',name="preTrained/B")
        assert(weights.shape[1]==biases.shape[0])
        
    def compute(self,inputs):
        assert(inputs.shape[1]==self.mW.shape[0])
        return tf.nn.relu(tf.matmul(inputs,self.mW)+self.mB,name='preTrainedReLU')
    
    def getOutputSize(self):
        return self.mW.shape[1]
    
class linearLayer:
    def __init__(self,inputSize,outputSize,use_biases=True):
#        self.mW = tf.Variable(tf.random_normal([inputSize,outputSize]))
        self.mW = tf.Variable(np.random.randn(inputSize,outputSize)*0.01,dtype='float32',name="linear/W")
        self.mUseBiases = use_biases
        if(use_biases):
            self.mB = tf.Variable(np.random.randn(outputSize)*0.01,dtype='float32',name="linear/B")
        
    def compute(self,inputs):
        outputs = tf.matmul(inputs,self.mW)
        if(self.mUseBiases):
            outputs += self.mB
        return outputs
    
    def getOutputSize(self):
        return self.mW.shape[1]
        
class preTrainedFC:
    def __init__(self):
        self.mLayers = []
    def addPreTrainedLayer(self,weights,biases):
        if(len(self.mLayers)>0):
            assert(self.mLayers[-1].getOutputSize() == weights.shape[0])
        self.mLayers.append(preTrainedLayer(weights,biases))
        
    def addLinearLayer(self,outputSize):
        self.mLayers.append(linearLayer(self.mLayers[-1].getOutputSize(),outputSize))
        
    def compute(self,inputs):
        outputs = inputs
        for i in range(len(self.mLayers)):
            outputs = self.mLayers[i].compute(outputs)
        return outputs
    