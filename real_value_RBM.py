# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 10:38:11 2018

@author: a010943
"""

import tensorflow as tf
import numpy as np
from getBatch import dataset
import matplotlib.pyplot as plt
from urllib import request
import gzip
import pickle
#import constantSpeedPredict as csp

even = np.array([0,2,4,6,8])
odd = np.array([1,3,5,7,9])

def sample_bernoulli(probabilities):
    assert(False)
    random = np.random.uniform(low=0,high=1,size=probabilities.shape)
    return np.float32(np.less(random,probabilities))

def ReLU(x,s=0):
    if (s==0):
        return np.fmax(0,x)
    else:
        r = np.random.randn(*(x.shape))*s
        return np.fmax(0,x+r)

def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))    

def getExamples(exNumber,size,data):
    dataExamples, truth, _ = data.get_batch(exNumber)
    
    dist = dataExamples[:,1:,:]-dataExamples[:,:-1,:]
    totalDist = np.sum(dist,axis=1)

    totalDist = np.tile(totalDist,[1,dataExamples.shape[1]+1])
    totalDist = np.reshape(totalDist,[-1,dataExamples.shape[1]+1,dataExamples.shape[2]])
    
    firstElementTile = np.tile(dataExamples[:,0,:],[1,dataExamples.shape[1]+1])
    firstElementTile = np.reshape(firstElementTile,[-1,dataExamples.shape[1]+1,dataExamples.shape[2]])
    
    dataExamples = np.concatenate((dataExamples, truth[:,np.newaxis,:]),axis=1)
    dataNormalized = np.divide((dataExamples - firstElementTile),totalDist)
    return dataNormalized[:,1:size+1,:], dataExamples[:,1:size+1,:]

class RBM:
    
    def __init__(self,n_hid, n_vis):
        self.mNHid = n_hid
        self.mNVis = n_vis
        self.mW = np.random.randn(n_hid,n_vis)*0.001
        self.mMomentumW = np.zeros([n_hid,n_vis])
        self.mMomentumBi = np.zeros([n_vis])
        self.mMomentumSi = np.zeros([n_vis])
        self.mMomentumBj = np.zeros([n_hid])
        self.mBi = np.random.randn(n_vis)*0.1
        self.mBj = np.random.randn(n_hid)*0.1
        self.mSi = np.ones(n_vis)#np.random.randn(n_vis)*0.1+
    
    def setWeights(self, weights, vis_std, vis_biases, hid_biases):
        self.mNHid = weights.shape[0]
        self.mNVis = weights.shape[1]
        self.mW = weights
        self.mBi = vis_biases
        self.mBj = hid_biases
        self.mSi = vis_std
        
    def setMomentums(self,momentum_weights, momentum_std, momentum_vis_biases, momentum_hid_biases):
        self.mMomentumBi = momentum_vis_biases
        self.mMomentumBj = momentum_hid_biases
        self.mMomentumSi = momentum_std
        self.mMomentumW = momentum_weights
    
    def visible_state_to_hidden_probabilities(self, visible_state,std=1):
        deltaEnergy = np.matmul(visible_state/self.mSi,np.transpose(self.mW)) + self.mBj
        return ReLU(deltaEnergy,std)#+np.random.randn(*deltaEnergy.shape)*np.square(np.mean(deltaEnergy,axis=0)))
    
    def hidden_state_to_visible_values(self, hidden_state, visible_state,std=1):
        deltaEnergy = np.matmul(hidden_state,self.mW)
        deltaEnergy = deltaEnergy/self.mSi+(self.mBi-visible_state)/(2*np.square(self.mSi))
        return ReLU(deltaEnergy,std)#+np.random.randn(*deltaEnergy.shape)*np.square(np.mean(deltaEnergy,axis=0)))#self.mBi+self.mSi*deltaEnergy

    def getHidden(self,data,nIter):
        for i in range(nIter):
            probaHidden = self.visible_state_to_hidden_probabilities(data,0) # get hidden state
            data = self.hidden_state_to_visible_values(probaHidden,data,0) # get visible state

        return probaHidden


#    def configuration_goodness(self, visible_state, hidden_state):
#        return np.mean(
#                np.sum(
#                        np.multiply
#                            (hidden_state,np.matmul(
#                                    self.mW,np.transpose(visible_state)))
#                            ,axis=0))

    def weights_gradient(self,visible_state, hidden_state):
        
        return np.matmul(np.transpose(visible_state/self.mSi),hidden_state)/visible_state.shape[0]

    def hidden_biases_gradient(self, hidden_state):
        
        return np.mean(hidden_state, axis=0)
    
    def visible_biases_gradient(self, visible_state):
        
        return self.mBi-np.mean(visible_state/np.square(self.mSi), axis=0)
    
    def visible_variance_gradient(self, visible_state, hidden_state):
        
        s1 = np.square((visible_state-self.mBi)/self.mSi)/self.mSi
        
        s2 = visible_state/np.square(self.mSi)      
        hw = np.matmul(hidden_state, self.mW)     
        s2 = s2*hw
        
        return np.mean(s1 - s2, axis = 0)

    def CDk(self, visible_data, k):
        
        probaHidden1 = self.visible_state_to_hidden_probabilities(visible_data)
#        hiddenState = sample_bernoulli(probaHidden1)
        valueVisible = self.hidden_state_to_visible_values(probaHidden1,visible_data)
        probaHidden2 = self.visible_state_to_hidden_probabilities(valueVisible)
        
        for i in range(k-1):            
#            hiddenState = sample_bernoulli(probaHidden2)
            valueVisible = self.hidden_state_to_visible_values(probaHidden2,valueVisible)
            probaHidden2 = self.visible_state_to_hidden_probabilities(valueVisible)

        positiveW = self.weights_gradient(visible_data,probaHidden1)
        negativeW = self.weights_gradient(valueVisible,probaHidden2)
        
        positiveBi = self.visible_biases_gradient(visible_data)
        negativeBi = self.visible_biases_gradient(valueVisible)
        
#        positiveSi = self.visible_variance_gradient(visible_data,probaHidden1)
#        negativeSi = self.visible_variance_gradient(valueVisible,probaHidden2)
        
        positiveBj = self.hidden_biases_gradient(probaHidden1)
        negativeBj = self.hidden_biases_gradient(probaHidden2)
        
#        return positiveW-negativeW, positiveSi-negativeSi, positiveBi-negativeBi, positiveBj-negativeBj
        return positiveW-negativeW, 0, positiveBi-negativeBi, positiveBj-negativeBj
    
    
    def predict(self, input_data, eps):
        
#        pred_temp =  (input_data[:,-2:]).copy()
        data_temp = np.zeros([input_data.shape[0],5,2])
        data_temp[:,:,0] = input_data[:,even]
        data_temp[:,:,1] = input_data[:,odd]
#        input_data[:,-2:] = csp.constantSpeed(data_temp,10)
        
#        while(np.mean(np.square(pred_temp-input_data[:,-2:]))>eps):  
        for i in range(1000):
#            pred_temp=(input_data[:,-2:]).copy()
            probaHidden = self.visible_state_to_hidden_probabilities(input_data) # get hidden state
#            sampleHidden = sample_bernoulli(probaHidden)
            valueVisible = self.hidden_state_to_visible_values(probaHidden,input_data) # get visible state
            input_data[:,-2:] = valueVisible[:,-2:]                            # update only prediction state
#            print('loss %1.10f' % np.mean(np.fabs(pred_temp-probaHV[:,-2:])))
        probaHidden = self.visible_state_to_hidden_probabilities(input_data) # get hidden state
        valueVisible = self.hidden_state_to_visible_values(probaHidden,input_data) # get visible state
        input_data[:,-2:] = valueVisible[:,-2:]
        return input_data[:,-2:]
    
    def generate(self,nIter=100):
        valueVisible =np.random.uniform(0,1,self.mNVis)
        
        for i in range(nIter):
            probaHidden = self.visible_state_to_hidden_probabilities(valueVisible,0) # get hidden state
            valueVisible = self.hidden_state_to_visible_values(probaHidden,valueVisible,0) # get visible state

        return valueVisible
        
    
    def train_step(self, data, learning_rate, momentum, CD_order):
        gradientW, gradientSi, gradientBi, gradientBj = self.CDk(data,CD_order)
        self.mMomentumW = momentum*self.mMomentumW + (1-momentum)*np.transpose(gradientW)
#        self.mMomentumSi = momentum*self.mMomentumSi + (1-momentum)*np.transpose(gradientSi)
        self.mMomentumBi = momentum*self.mMomentumBi + (1-momentum)*np.transpose(gradientBi)
        self.mMomentumBj = momentum*self.mMomentumBj + (1-momentum)*np.transpose(gradientBj)
        self.mW += learning_rate*self.mMomentumW
        self.mBi += learning_rate*self.mMomentumBi
#        self.mSi += learning_rate*self.mMomentumSi
        self.mBj += learning_rate*self.mMomentumBj
        
    def save(self,num_iter_all,name="RBM_save"):
        np.savez_compressed("./save/"+name+".npz",weights = [self.mW, self.mMomentumW],
                            visSB = [self.mSi, self.mBi, self.mMomentumSi, self.mMomentumBi],
                            hidB = [self.mBj, self.mMomentumBj],
                            size = [self.mNHid, self.mNVis],
                            nIter = num_iter_all)

        
#batch_size = 100        
#num_batch_per_epoch = 200
#num_epoch = 50
#myRBM = RBM(128,12)
#startingCDorder = 2
#
#num_iter_all = 10000

#
#data = dataset('./vehicle-trajectory-data/vehicle-trajectory-data-4/0400pm-0415pm/',
#               'trajectories-0400-0415.csv',
#               memoryLength = 5, horizon = 1)
#n_iter = 0
#try:
#    with np.load("./save/RBM_save.npz") as reloadedvalues:
#        size = reloadedvalues["size"]
#        
#        assert(size[0]==myRBM.mNHid)
#        assert(size[1]==myRBM.mNVis)
#        myRBM.setWeights(reloadedvalues["weights"][0],
#                         reloadedvalues["visSB"][0],
#                         reloadedvalues["visSB"][1],
#                         reloadedvalues["hidB"][0])
#
#        n_iter = reloadedvalues["nIter"]
#        myRBM.setMomentums(reloadedvalues["weights"][1],
#                     reloadedvalues["visSB"][2],
#                     reloadedvalues["visSB"][3],
#                     reloadedvalues["hidB"][1])
#        print("Restored weights and momentums")
#       
#except:
#    n_iter = 0
#    
#while(n_iter < num_iter_all):
#    print("Creating new batch of data")
#    inputs_batch_epoch, targets_batch_epoch, neighborhood_batch_epoch = data.get_batch(batch_size*num_batch_per_epoch)
#    inputs_batch_epoch = (inputs_batch_epoch +10)/20
#    targets_batch_epoch = (targets_batch_epoch +10)/20
#    for i in range(num_epoch):
##        print("Restart an epoch")
#        shuffling = np.random.permutation(inputs_batch_epoch.shape[0])
#        inputs_batch_epoch = inputs_batch_epoch[shuffling,:,:]
#        targets_batch_epoch = targets_batch_epoch[shuffling,:]
#        neighborhood_batch_epoch = neighborhood_batch_epoch[shuffling,:]
#        for j in range(num_batch_per_epoch):
#            n_iter +=1
#            if(n_iter > num_iter_all):
#                break
#            inputs_batch = inputs_batch_epoch[j*batch_size:(j+1)*batch_size,:,:]
#            targets_batch = targets_batch_epoch[j*batch_size:(j+1)*batch_size,:]
#            targets_batch = targets_batch[:,0:2]
#            
#            
#            inputs_batch = np.reshape(inputs_batch,[-1,inputs_batch.shape[1]*inputs_batch.shape[2]])
#            visible = np.float32(np.concatenate((inputs_batch, targets_batch),axis=1))
#            myRBM.train_step(visible,0.1,0.9,1)#int(i/5)+startingCDorder)
#            if n_iter%1000==0:
#                print('iter %d' % n_iter)
#                
#myRBM.save(num_iter_all) 
#
#W = myRBM.mW   
#bi = myRBM.mBi   
#bj = myRBM.mBj   
#si = myRBM.mSi   
#
#
#index = np.random.randint(0,50,1)
#x = myRBM.mW[index,even]
#y = myRBM.mW[index,odd]
#
#plt.plot(x,y)
#plt.plot(myRBM.mW[index,10],myRBM.mW[index,11],'+')
#
#plt.close()
#
#valueGenerated = myRBM.generate()
#
#plt.plot(valueGenerated[even],valueGenerated[odd])
#plt.plot(valueGenerated[10],valueGenerated[11],'+')
#
#plt.close()
#
#example_input, truth, neigh= data.get_example()
#example_input = (example_input[:,:,0:2]+10)/20
#truth = (truth+10)/20
#n = example_input.shape[0]
#m1 = example_input.shape[1]*example_input.shape[2]
#m2 = (example_input.shape[1]+1)*example_input.shape[2]
#reshaped_input = np.zeros([n,m2])
#
#reshaped_input[:,0:m1] = np.reshape(example_input,[n,m1])
#        
#prediction = myRBM.predict(reshaped_input, 1e-9)
#
#indexToPlot = 100
#plt.plot(prediction[:,0],prediction[:,1],'red',truth[:,0,0],truth[:,0,1],'green')
#plt.plot(prediction[indexToPlot,0],prediction[indexToPlot,1],'+',
#         truth[indexToPlot,0,0],truth[indexToPlot,0,1],'.',
#         example_input[indexToPlot,0,0],example_input[indexToPlot,0,1],'x',
#         example_input[indexToPlot,1,0],example_input[indexToPlot,1,1],'x',
#         example_input[indexToPlot,2,0],example_input[indexToPlot,2,1],'x',
#         example_input[indexToPlot,3,0],example_input[indexToPlot,3,1],'x',
#         example_input[indexToPlot,4,0],example_input[indexToPlot,4,1],'x',markersize=20)       
#
#
#np.savez_compressed("./save/predictions.npz",pred = prediction,targets=truth,inputs=example_input)          

# Test to see if RBM is working on good data: it does work
#        
def load():
    with open("./data/mnist.pkl",'rb') as f:
        mnist = pickle.load(f)
    return mnist["training_images"], mnist["training_labels"], mnist["test_images"], mnist["test_labels"]


training, labels, test, test_labels = load()

indexBatch = 0
def getBatch(size):
    global index
    batch = np.float32(training[indexBatch:indexBatch+size])/256
    index = np.mod(indexBatch+size,training.shape[0])
    return batch
    
mnistRBM = RBM(100,training.shape[1])


try:
    with np.load("./save/RBM_save.npz") as reloadedvalues:
        size = reloadedvalues["size"]
        
        assert(size[0]==mnistRBM.mNHid)
        assert(size[1]==mnistRBM.mNVis)
        mnistRBM.setWeights(reloadedvalues["weights"][0],
                         reloadedvalues["visSB"][0],
                         reloadedvalues["visSB"][1],
                         reloadedvalues["hidB"][0])
    
        n_iter = reloadedvalues["nIter"]
        mnistRBM.setMomentums(reloadedvalues["weights"][1],
                     reloadedvalues["visSB"][2],
                     reloadedvalues["visSB"][3],
                     reloadedvalues["hidB"][1])
        print("Restored weights and momentums")
except:
    for i in range(100):
        batch = getBatch(100)
        mnistRBM.train_step(batch,0.01,0.1,1)
        if i%100 == 0:
            print('iteration %d' % i)
    for i in range(10000):
        batch = getBatch(100)
        order = np.abs(int(np.random.randn(1)*3))+1
        mnistRBM.train_step(batch,0.01,0.9,order)
        if i%100 == 0:
            print('iteration %d' % i)

mnistRBM.save(10000)

W  = mnistRBM.mW   
bi = mnistRBM.mBi   
bj = mnistRBM.mBj   
si = mnistRBM.mSi  
    
index = np.random.randint(0,100,1)    
print(index)
plt.imshow(np.reshape(mnistRBM.mW[index,:],[28,28]))
test = mnistRBM.mW
ic = 10
jc = 10
f, axes = plt.subplots(nrows=jc, ncols=ic, figsize=(28, 28))
for j in range(jc):
    for i in range(ic):
        axes[i,j].imshow(np.reshape(mnistRBM.mW[i+ic*j,:],[28,28]), cmap='gray')
        axes[i,j].axis('off')
        
genData = mnistRBM.generate(50) 
plt.imshow(np.reshape(genData,[28,28]))


mnistRBM2 = RBM(100,100)


try:
    with np.load("./save/RBM2_save.npz") as reloadedvalues:
        size = reloadedvalues["size"]
        
        assert(size[0]==mnistRBM2.mNHid)
        assert(size[1]==mnistRBM2.mNVis)
        mnistRBM2.setWeights(reloadedvalues["weights"][0],
                         reloadedvalues["visSB"][0],
                         reloadedvalues["visSB"][1],
                         reloadedvalues["hidB"][0])
    
        n_iter = reloadedvalues["nIter"]
        mnistRBM2.setMomentums(reloadedvalues["weights"][1],
                     reloadedvalues["visSB"][2],
                     reloadedvalues["visSB"][3],
                     reloadedvalues["hidB"][1])
        print("Restored weights and momentums")
except:
    for i in range(100):
        batch = getBatch(100)
        hiddenBatch = mnistRBM.getHidden(batch,3)
        mnistRBM2.train_step(hiddenBatch,0.001,0.1,1)
        if i%100 == 0:
            print('iteration %d' % i)
    for i in range(10000):
        batch = getBatch(100)
        order = np.abs(int(np.random.randn(1)*3))+1
        hiddenBatch = mnistRBM.getHidden(batch,order)
        mnistRBM2.train_step(hiddenBatch,0.001,0.9,order)
        if i%100 == 0:
            print('iteration %d' % i)

mnistRBM2.save(10000,"RBM2_save")

index = np.random.randint(0,100,1)    
plt.imshow(np.reshape(mnistRBM2.mW[index,:],[10,10]))
ic = 10
jc = 10
f, axes = plt.subplots(nrows=jc, ncols=ic, figsize=(10, 10))
for j in range(jc):
    for i in range(ic):
        axes[i,j].imshow(np.reshape(mnistRBM2.mW[i+ic*j,:],[10,10]), cmap='gray')
        axes[i,j].axis('off')