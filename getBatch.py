#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 17:25:05 2018

@author: jean
"""


import numpy as np
import matplotlib.pyplot as plt
from enum import Enum
import time
import bisect

from scipy import spatial



class feature(int,Enum):
    Vehicle_ID      =0
    Frame_ID        =1
    Total_Frames    =2
    Global_Time     =3
    Local_X         =4
    Local_Y         =5
    Global_X        =6
    Global_Y        =7
    v_Length        =8
    v_Width         =9
    v_Class         =10
    v_Vel           =11
    v_Acc           =12
    Lane_ID         =13
    O_Zone          =14
    D_Zone          =15
    Int_ID          =16
    Section_ID      =17
    Direction       =18
    Movement        =19
    Preceding       =20
    Following       =21
    Space_Headway   =22
    Time_Headway    =23

#Extract the data from a single vehicle


#Returns the time window in which the given vehicle data were taken
def getTimeBoundary(vehicleData):
    timeMin = np.min(vehicleData[:,feature.Global_Time])
    timeMax = np.max(vehicleData[:,feature.Global_Time])
    return timeMin, timeMax
	
def getDistance2(relativeData):
    dx = relativeData[:,feature.Global_X]
    dy = relativeData[:,feature.Global_Y]
    return dx*dx+dy*dy

def getVehicleDataFromID(vehicle_ID,dataIDSorted):
        index0 = bisect.bisect_left(dataIDSorted[:,feature.Vehicle_ID],vehicle_ID)
        index1 = bisect.bisect_right(dataIDSorted[:,feature.Vehicle_ID],vehicle_ID)
        return dataIDSorted[index0:index1,:]
    
def getDataAtTime(t,dataTimeSorted,dt,memoryLength=1):
    index0 = bisect.bisect_left(dataTimeSorted[:,feature.Global_Time],t-dt*(memoryLength-1)-dt/2)
    index1 = bisect.bisect_right(dataTimeSorted[:,feature.Global_Time],t+dt/2)
    return dataTimeSorted[index0:index1,:]

def normalize(data,whichData):    
    mini = np.min(data[:,whichData],axis=0)
    mean = np.mean(data[:,whichData],axis=0)
    std = np.std(data[:,whichData],axis=0)
    for i,item in enumerate(whichData):
        data[:,item] = (data[:,item] - mean[i])/std[i]
    return data, mean, std, mini

def unnormalize(data,mean,std,mini):
    n = min([data.shape[1],mean.shape[0],std.shape[0]])
    for i in range(n):
        data[:,i] *= std[i]
        data[:,i] += mean[i]-mini[i]
    return data

def getDt(dataTimeSorted):
    i = 0
    dt = 0
    while(dt==0):
        dt = dataTimeSorted[i+1,feature.Global_Time]-dataTimeSorted[i,feature.Global_Time]
        i += 1
    return dt

class dataset: 
    def __init__(self,fileLocation,fileName,memoryLength=1,horizon=1,mean=[],std=[],mini=[]):
        self.fileLocation = fileLocation #'./vehicle-trajectory-data/vehicle-trajectory-data-4/0400pm-0415pm/'
        self.fileName = fileName #'trajectories-0400-0415.csv'
        meterPerFoot = 0.3048
        rangeMeasure = 100/meterPerFoot
        data = np.loadtxt(open(fileLocation+fileName,"rb"),delimiter=",",skiprows=1)
        self.whichDataObs = [feature.Global_X, feature.Global_Y]
        self.whichDataEgo = []#feature.Global_X, feature.Global_Y,

        if(len(mean)!=len(self.whichDataObs) or len(std)!=len(self.whichDataObs)):
            data,self.mean,self.std,self.mini = normalize(data,self.whichDataObs)
        else:
            print('Normalization imposed')
            for i,item in enumerate(self.whichDataObs):
                data[:,item] = (data[:,item] - mean[i])/std[i]
            self.mean = mean
            self.std = std
            self.mini = mini
        self.xyCoef = self.std[1]/self.std[0]
        #sort by time then by ID
        self.dataTimeSorted = data[np.lexsort((data[:,feature.Vehicle_ID], data[:,feature.Global_Time]))]
        self.dt = getDt(self.dataTimeSorted)
        #sort by ID then by time
        self.dataIDSorted = data[np.lexsort((data[:,feature.Global_Time], data[:,feature.Vehicle_ID]))]
                
        self.vehiclesIDlist = np.unique(data[:,feature.Vehicle_ID])
        self.iterPredict = 10*horizon
        
        self.maxDistance = np.sqrt(rangeMeasure*rangeMeasure)
        self.memoryLength = memoryLength
        self.dataKDTree,self.timeKDTree,self.indexKDTree = self._setKDTree()
        
        print('dt = %d' % (self.dt))
        
    def _setKDTree(self):
        coorSpatial = [feature.Global_X,feature.Global_Y]
        treeList = []
        timeList = []
        indexList = []
        dataSize = self.dataTimeSorted.shape[0]-1
        indexR = 0
        indexL = 0
        while(indexR < dataSize):
            t = self.dataTimeSorted[indexL,feature.Global_Time]
            indexR = np.searchsorted(
                self.dataTimeSorted[:,feature.Global_Time],
                t+self.dt/2,side = 'right')
            if(indexR>indexL+1):
#                print('indexL %d indexR %d'%(indexL,indexR))
#                print(self.dataTimeSorted[indexL:indexR,coorSpatial])
                tree = spatial.cKDTree(self.dataTimeSorted[indexL:indexR,coorSpatial])
                treeList.append(tree)
                timeList.append(t)
                indexList.append(indexL)
            indexL = indexR
        return treeList, np.uint64(np.array(timeList)), np.uint32(np.array(indexList))

    def unnormalize(self,data):
        meterPerFoot = 0.3048
        return unnormalize(data,self.mean,self.std,self.mini)*meterPerFoot
    
    def get_inputSize(self):
        return (len(self.whichDataObs)+len(self.whichDataEgo)),self.memoryLength
    
    def get_outputSize(self):
        return len(self.whichDataObs)
    
    def get_dataTimeSorted(self):
        return self.dataTimeSorted
    
    def get_dataIDSorted(self):
        return self.dataIDSorted
    
    def get_vehiclesIDlist(self):
        return self.vehiclesIDlist
    
    def getVehicleDataFromID(self,vehicle_ID):
        index0 = bisect.bisect_left(self.dataIDSorted[:,feature.Vehicle_ID],vehicle_ID)
        index1 = bisect.bisect_right(self.dataIDSorted[:,feature.Vehicle_ID],vehicle_ID)
        return self.dataIDSorted[index0:index1,:]
    
    def getDataAtTime(self,t):
        index0 = bisect.bisect_left(self.dataTimeSorted[:,feature.Global_Time],t-self.dt*(self.memoryLength-1)-self.dt/2)
        index1 = bisect.bisect_right(self.dataTimeSorted[:,feature.Global_Time],t+self.dt/2)
        return self.dataTimeSorted[index0:index1,:]
    
    def getTreeAtTime(self,t):
        index = bisect.bisect_left(self.timeKDTree,np.uint64(t))
        return self.dataKDTree[index],self.indexKDTree[index]
    
    def customQueryBall(self,time,point,radius):
        try:
            treeAtTime, index = self.getTreeAtTime(time)
            localIndices = treeAtTime.query_ball_point(point,radius)
            return localIndices+index
        except:
            return []
        return []
    
    def getNeighborhood(self,time,position,radius,ID):      
        indicesAtTime02 = self.customQueryBall(time,position+[radius,radius],radius)
        indicesAtTime22 = self.customQueryBall(time,position+[radius,-radius],radius)
        indicesAtTime00 = self.customQueryBall(time,position+[-radius,radius],radius)
        indicesAtTime20 = self.customQueryBall(time,position+[-radius,-radius],radius)
        indicesAtTime12 = self.customQueryBall(time,position+[radius,0],radius)
        indicesAtTime01 = self.customQueryBall(time,position+[0,radius],radius)
        indicesAtTime10 = self.customQueryBall(time,position+[-radius,0],radius)
        indicesAtTime21 = self.customQueryBall(time,position+[0,-radius],radius)
        indicesAtTime00 = self.customQueryBall(time,position,radius)
        neighborHood = np.zeros([3,3])
#        assert(np.sum(np.equal(indicesAtTime00,ID))>0)
#        assert(np.sum(np.not_equal(indicesAtTime00,ID))==0)
        neighborHood[0,0] = np.sum(np.not_equal(indicesAtTime00,ID))>0
        neighborHood[0,1] = np.sum(np.not_equal(indicesAtTime01,ID))>0
        neighborHood[0,2] = np.sum(np.not_equal(indicesAtTime02,ID))>0
        neighborHood[1,0] = np.sum(np.not_equal(indicesAtTime10,ID))>0
        neighborHood[1,1] = 0
        neighborHood[1,2] = np.sum(np.not_equal(indicesAtTime12,ID))>0
        neighborHood[2,0] = np.sum(np.not_equal(indicesAtTime20,ID))>0
        neighborHood[2,1] = np.sum(np.not_equal(indicesAtTime21,ID))>0
        neighborHood[2,2] = np.sum(np.not_equal(indicesAtTime22,ID))>0
        return neighborHood
        

        
        
    def getRandomDataAtTime(self,dataRef,t):
        dataRefID = dataRef[0,feature.Vehicle_ID]
        xyFeatures = [feature.Global_X,feature.Global_Y]
        indicesAtTime = self.customQueryBall(t,dataRef[0,xyFeatures],self.maxDistance)
        
        vehicleDataAtTime = np.empty(0)
        vehicleDataAtNextTime = np.empty(0)
    
        nIndices = len(indicesAtTime)
        if(nIndices<2):
            return np.empty(0),np.empty(0)
        else:
            np.random.shuffle(indicesAtTime)
            i = -1
            notEnoughTime = True
            while(notEnoughTime and i < nIndices-1):
                i += 1
                randID =  self.dataTimeSorted[indicesAtTime[i],feature.Vehicle_ID]
                while(randID == dataRefID and i < nIndices-1):
                    i += 1
                    randID =  self.dataTimeSorted[indicesAtTime[i],feature.Vehicle_ID]
                if(randID == dataRefID and i == nIndices-1):
                    return np.empty(0),np.empty(0)
                else:
                    vehicleData = self.getVehicleDataFromID(randID)
                    vehicleDataAtTime = getDataAtTime(t,vehicleData,self.dt,self.memoryLength)
                    vehicleDataAtNextTime = getDataAtTime(t+self.iterPredict*self.dt,vehicleData,self.dt)
                    if(vehicleDataAtNextTime.shape[0]>0):
                        notEnoughTime = vehicleDataAtTime.shape[0] < self.memoryLength \
                                     or vehicleDataAtNextTime[0,feature.Global_Time]<t+(self.iterPredict-1)*self.dt
        return vehicleDataAtTime, vehicleDataAtNextTime

    def get_batch(self,size):
        nChannels,nMemory = self.get_inputSize()
        inputs = np.zeros([size,nMemory,nChannels])
        neighborhoods = np.zeros([size,3,3])
        targets = np.zeros([size,self.get_outputSize()])
        idListSize = self.vehiclesIDlist.shape[0]
        xyFeatures = [feature.Global_X,feature.Global_Y]
        meterPerFoot = 0.3048
        i = 0
        while i < size:
            randEgoID = int(self.vehiclesIDlist[np.random.randint(0,idListSize,1)[0]])
            dataEgo = self.getVehicleDataFromID(randEgoID)
                        
#            dataEgo = dataEgo[dataEgo[:,feature.Global_Time].argsort()]
            timeMin = dataEgo[0,feature.Global_Time]
            timeMax = dataEgo[-1:,feature.Global_Time][0]
            if (timeMax-timeMin < self.memoryLength*self.dt+self.iterPredict*self.dt):
                continue
            randTime = np.random.uniform(timeMin+self.memoryLength*self.dt,timeMax-self.iterPredict*self.dt)
            
            dataEgoAtTime = getDataAtTime(randTime,dataEgo,self.dt,self.memoryLength)
            dataEgoAtNextTime = getDataAtTime(randTime+self.iterPredict*self.dt,dataEgo,self.dt,self.memoryLength)
                    
            dataAtTime, dataAtNextTime = self.getRandomDataAtTime(dataEgoAtTime,randTime)
            testedTimes = 0
#            print('time gap: %d' %(dataEgoAtNextTime[0,feature.Global_Time]-dataEgoAtTime[0,feature.Global_Time]))
            while( (len(dataAtTime) < 1 or len(dataAtNextTime)<1) and testedTimes < 10):
                randTime = np.random.uniform(timeMin,timeMax)
            
                dataEgoAtTime = getDataAtTime(randTime,dataEgo,self.dt,self.memoryLength)
                dataEgoAtNextTime = getDataAtTime(randTime+self.iterPredict*self.dt,dataEgo,self.dt,self.memoryLength)
                
                dataAtTime, dataAtNextTime = self.getRandomDataAtTime(dataEgoAtTime,randTime)
                testedTimes = testedTimes + 1
            if(testedTimes == 10):
                print('continue 1')
                continue
            try:
                ID = dataAtTime[0,feature.Vehicle_ID]
                neighborhoods[i,:,:] = self.getNeighborhood(randTime,dataAtTime[-1,xyFeatures],5/meterPerFoot,ID)
                relativeDataAtTime = dataAtTime[-self.memoryLength:,self.whichDataObs]-dataEgoAtTime[-self.memoryLength:,self.whichDataObs]
                relativeDataAtNextTime = (dataAtNextTime[-1:,self.whichDataObs]-dataEgoAtNextTime[-1:,self.whichDataObs])[0,:]
#                print('Id of the data at time %d' %(dataAtTime[-1:,feature.Vehicle_ID]))
#                print('Id of the data at next time %d' %(dataAtNextTime[-1:,feature.Vehicle_ID]))
                inputs[i,:,:] = np.concatenate((relativeDataAtTime,dataEgoAtTime[-self.memoryLength:,self.whichDataEgo]),axis=1)
                targets[i,:] = relativeDataAtNextTime
            except:
#                print('continue 2')
                continue
            i += 1
                
        return inputs, targets, neighborhoods.astype(np.float32)
    
    def get_example(self):
        idListSize = self.vehiclesIDlist.shape[0]    
        randEgoID = int(self.vehiclesIDlist[np.random.randint(0,idListSize,1)[0]])
        dataEgo = self.getVehicleDataFromID(randEgoID)
        meterPerFoot = 0.3048

        timeMin, timeMax = getTimeBoundary(dataEgo)
        dataEgo = dataEgo[dataEgo[:,feature.Global_Time].argsort()]
        
        randTime = np.random.uniform(timeMin,timeMax)
        
        dataEgoAtTime = getDataAtTime(randTime,dataEgo,self.dt,self.memoryLength)
        
        dataAtTime, dataAtNextTime = self.getRandomDataAtTime(dataEgoAtTime,randTime)
        
        dataObserved = self.getVehicleDataFromID(dataAtTime[0,feature.Vehicle_ID])
        
        dataObserved = dataObserved[dataObserved[:,feature.Global_Time].argsort()]
        dataEgo = dataEgo[dataEgo[:,feature.Global_Time].argsort()]
        
        timeBeg = max(dataObserved[ 0,feature.Global_Time],timeMin)
        timeEnd = min(dataObserved[-1,feature.Global_Time],timeMax)
        
        index11 = np.searchsorted(dataEgo[:,feature.Global_Time],timeBeg,side='left')
        index12 = np.searchsorted(dataObserved[:,feature.Global_Time],timeBeg,side='left')
        index21 = np.searchsorted(dataEgo[:,feature.Global_Time],timeEnd,side='right')
        index22 = np.searchsorted(dataObserved[:,feature.Global_Time],timeEnd,side='right')
        
        if (index21-index11 != index22-index12):
            print('Il y a un probleme de taille ... ha...ha')
        if(index21-index11 < self.iterPredict):
            return self.get_example()
        
        dataEgoTemp      = np.zeros((index21-index11-self.memoryLength+1,self.memoryLength,len(self.whichDataObs)))
        dataObservedTemp = np.zeros((index22-index12-self.memoryLength+1,self.memoryLength,len(self.whichDataObs)))
        
        for i in range(self.memoryLength):
            dataEgoTemp[:,i,:]      =      dataEgo[index11+i:index21-self.memoryLength+1+i,self.whichDataObs]
            dataObservedTemp[:,i,:] = dataObserved[index12+i:index22-self.memoryLength+1+i,self.whichDataObs]
        
        dataEgo = dataEgoTemp
        neighborhoods = np.zeros([dataEgoTemp.shape[0],3,3])
        for i in range(dataObservedTemp.shape[0]):
            ID = dataAtTime[0,feature.Vehicle_ID]
            neighborhoods[i,:,:] = self.getNeighborhood(randTime,dataObservedTemp[i,-1,0:2],5/meterPerFoot,ID)
            
        dataObserved = dataObservedTemp - dataEgoTemp
        return np.concatenate((dataObserved[:-self.iterPredict,:,:],dataEgo[:-self.iterPredict,:,-len(self.whichDataEgo):]),axis=2),\
               dataObserved.copy()[self.iterPredict:,-1:,-len(self.whichDataObs):],\
               neighborhoods[:-self.iterPredict,:,:]
        
    
#data = dataset('./vehicle-trajectory-data/vehicle-trajectory-data-4/0400pm-0415pm/',
#               'trajectories-0400-0415.csv',
#               memoryLength=5,horizon=1)
#
#print('Dataset loaded')
#
#time0 = time.time()*1000.0
#inputs,targets = data.get_batch(100)
#print('Time to get a batch: %.2f ms' % ((time.time()*1000.0-time0)))
#
#
#plt.plot(inputs[0,:,0],inputs[0,:,1],'x')
#plt.plot(targets[0,0],targets[0,1],'.',markersize=20)
#
##
#example_input, truth = data.get_example()
#
#truth[:,0,:] = data.unnormalize(truth[:,0,:])
#for i in range(data.memoryLength):
#    example_input[:,i,0:4] = data.unnormalize(example_input[:,i,0:4])
#    example_input[:,i,4:6] = data.unnormalize(example_input[:,i,4:6])
#
#plt.plot(truth[:,0,0],truth[:,0,1],'green',
#         example_input[:,-1:,0],example_input[:,-1:,1])
#indexToPlot = 200
#plt.plot(truth[indexToPlot,0,0],truth[indexToPlot,0,1],'.',
#         example_input[indexToPlot,-1:,0],example_input[indexToPlot,-1:,1],'x',markersize=20)

