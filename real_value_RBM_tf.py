# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 23:20:08 2018

@author: a010943
"""

import tensorflow as tf
import numpy as np
#from getBatch import dataset
import matplotlib.pyplot as plt
import pickle
from preTrainedFC import preTrainedFC as ptfc

even = np.array([0,2,4,6,8])
odd = np.array([1,3,5,7,9])

def sample_bernoulli(probabilities):
    assert(False)
    random = tf.random.uniform(low=0,high=1,size=probabilities.shape)
    return tf.float32(tf.less(random,probabilities))

def ReLU(x,s=0):
    if (s==0):
        return tf.maximum(0.0,x)
    else:
        r = tf.random_uniform(tf.shape(x),0,s)
        return tf.maximum(0.0,x+r)

def sigmoid(x):
    return 1.0 / (1.0 + tf.exp(-x))    

#def getExamples(exNumber,size,data):
#    dataExamples, truth, _ = data.get_batch(exNumber)
#    
#    dist = dataExamples[:,1:,:]-dataExamples[:,:-1,:]
#    totalDist = tf.sum(dist,axis=1)
#
#    totalDist = tf.tile(totalDist,[1,dataExamples.shape[1]+1])
#    totalDist = tf.reshape(totalDist,[-1,dataExamples.shape[1]+1,dataExamples.shape[2]])
#    
#    firstElementTile = tf.tile(dataExamples[:,0,:],[1,dataExamples.shape[1]+1])
#    firstElementTile = tf.reshape(firstElementTile,[-1,dataExamples.shape[1]+1,dataExamples.shape[2]])
#    
#    dataExamples = tf.concatenate((dataExamples, truth[:,tf.newaxis,:]),axis=1)
#    dataNormalized = tf.divide((dataExamples - firstElementTile),totalDist)
#    return dataNormalized[:,1:size+1,:], dataExamples[:,1:size+1,:]

class RBM:
    
    def __init__(self,n_hid, n_vis):
        self.mNHid = n_hid
        self.mNVis = n_vis
        self.mW = tf.Variable(tf.random_normal([n_hid,n_vis],0.0,0.001))
        self.mMomentumW = tf.Variable(tf.zeros([n_hid,n_vis]))
        self.mMomentumBi = tf.Variable(tf.zeros([n_vis]))
        self.mMomentumSi = tf.Variable(tf.zeros([n_vis]))
        self.mMomentumBj = tf.Variable(tf.zeros([n_hid]))
        self.mBi = tf.Variable(tf.random_normal([n_vis],0.0,0.1))
        self.mBj = tf.Variable(tf.random_normal([n_hid],0.0,0.1))
        self.mSi = tf.ones([n_vis])#tf.random.randn(n_vis)*0.1+
    
    def setWeights(self, weights, vis_std, vis_biases, hid_biases):
        self.mNHid = weights.shape[0]
        self.mNVis = weights.shape[1]
        self.mW = tf.Variable(weights)
        self.mBi = tf.Variable(vis_biases)
        self.mBj = tf.Variable(hid_biases)
#        self.mSi = tf.Constant(vis_std)
        
    def setMomentums(self,momentum_weights, momentum_std, momentum_vis_biases, momentum_hid_biases):
        self.mMomentumBi = momentum_vis_biases
        self.mMomentumBj = momentum_hid_biases
#        self.mMomentumSi = momentum_std
        self.mMomentumW = momentum_weights
    
    def visible_state_to_hidden_probabilities(self, visible_state,std=1):
        deltaEnergy = tf.matmul(visible_state/self.mSi,tf.transpose(self.mW)) + self.mBj
        return ReLU(deltaEnergy,std)#+tf.random.randn(*deltaEnergy.shape)*tf.square(tf.mean(deltaEnergy,axis=0)))
    
    def hidden_state_to_visible_values(self, hidden_state, visible_state,std=1):
        deltaEnergy = tf.matmul(hidden_state,self.mW)
        deltaEnergy = deltaEnergy/self.mSi+(self.mBi-visible_state)/(2*tf.square(self.mSi))
        return ReLU(deltaEnergy,std)#+tf.random.randn(*deltaEnergy.shape)*tf.square(tf.mean(deltaEnergy,axis=0)))#self.mBi+self.mSi*deltaEnergy

    def getHidden(self,data,nIter):
        for i in range(nIter):
            probaHidden = self.visible_state_to_hidden_probabilities(data,0) # get hidden state
            data = self.hidden_state_to_visible_values(probaHidden,data,0) # get visible state

        return probaHidden


#    def configuration_goodness(self, visible_state, hidden_state):
#        return tf.mean(
#                tf.sum(
#                        tf.multiply
#                            (hidden_state,tf.matmul(
#                                    self.mW,tf.transpose(visible_state)))
#                            ,axis=0))

    def weights_gradient(self,visible_state, hidden_state):
        
        return tf.matmul(tf.transpose(visible_state/self.mSi),hidden_state)/tf.cast(tf.shape(visible_state)[0],tf.float32)

    def hidden_biases_gradient(self, hidden_state):
        
        return tf.reduce_mean(hidden_state, axis=0)
    
    def visible_biases_gradient(self, visible_state):
        
        return self.mBi-tf.reduce_mean(visible_state/tf.square(self.mSi), axis=0)
    
    def visible_variance_gradient(self, visible_state, hidden_state):
        
        s1 = tf.square((visible_state-self.mBi)/self.mSi)/self.mSi
        
        s2 = visible_state/tf.square(self.mSi)      
        hw = tf.matmul(hidden_state, self.mW)     
        s2 = s2*hw
        
        return tf.mean(s1 - s2, axis = 0)

    def CDk(self, visible_data, k):
        
        probaHidden1 = self.visible_state_to_hidden_probabilities(visible_data)
        valueVisible = self.hidden_state_to_visible_values(probaHidden1,visible_data)
        probaHidden2 = self.visible_state_to_hidden_probabilities(valueVisible)
#        for i in range(k.eval(session=sess)-1):            
##            hiddenState = sample_bernoulli(probaHidden2)
#            valueVisible = self.hidden_state_to_visible_values(probaHidden2,valueVisible)
#            probaHidden2 = self.visible_state_to_hidden_probabilities(valueVisible)

        positiveW = self.weights_gradient(visible_data,probaHidden1)
        negativeW = self.weights_gradient(valueVisible,probaHidden2)
        
        positiveBi = self.visible_biases_gradient(visible_data)
        negativeBi = self.visible_biases_gradient(valueVisible)
        
#        positiveSi = self.visible_variance_gradient(visible_data,probaHidden1)
#        negativeSi = self.visible_variance_gradient(valueVisible,probaHidden2)
        
        positiveBj = self.hidden_biases_gradient(probaHidden1)
        negativeBj = self.hidden_biases_gradient(probaHidden2)
        
#        return positiveW-negativeW, positiveSi-negativeSi, positiveBi-negativeBi, positiveBj-negativeBj
        return positiveW-negativeW, 0, positiveBi-negativeBi, positiveBj-negativeBj
    
    
    def predict(self, input_data, eps):
        
#        pred_temp =  (input_data[:,-2:]).copy()
        data_temp = tf.zeros([input_data.shape[0],5,2])
        data_temp[:,:,0] = input_data[:,even]
        data_temp[:,:,1] = input_data[:,odd]
#        input_data[:,-2:] = csp.constantSpeed(data_temp,10)
        
#        while(tf.mean(tf.square(pred_temp-input_data[:,-2:]))>eps):  
        for i in range(1000):
#            pred_temp=(input_data[:,-2:]).copy()
            probaHidden = self.visible_state_to_hidden_probabilities(input_data,0) # get hidden state
#            sampleHidden = sample_bernoulli(probaHidden)
            valueVisible = self.hidden_state_to_visible_values(probaHidden,input_data,0) # get visible state
            input_data[:,-2:] = valueVisible[:,-2:]                            # update only prediction state
#            print('loss %1.10f' % tf.mean(tf.fabs(pred_temp-probaHV[:,-2:])))
        probaHidden = self.visible_state_to_hidden_probabilities(input_data,0) # get hidden state
        valueVisible = self.hidden_state_to_visible_values(probaHidden,input_data,0) # get visible state
        input_data[:,-2:] = valueVisible[:,-2:]
        return input_data[:,-2:]
    
    def generate(self,session,nIter=100):
        valueVisible =tf.Variable(tf.random_uniform([1,self.mNVis],0.0,1.0))
        session.run(valueVisible.initializer)
        for i in range(nIter):
            probaHidden = self.visible_state_to_hidden_probabilities(valueVisible,((nIter-i)/nIter)**2) # get hidden state
            valueVisible = self.hidden_state_to_visible_values(probaHidden,valueVisible,((nIter-i)/nIter)**2) # get visible state

        probaHidden = self.visible_state_to_hidden_probabilities(valueVisible,0) # get hidden state
        valueVisible = self.hidden_state_to_visible_values(probaHidden,valueVisible,0) # get visible state
        return valueVisible.eval(session=session)
        
    
    def train_step(self, data, learning_rate, momentum, CD_order):
        gradientW, gradientSi, gradientBi, gradientBj = self.CDk(data,CD_order)
        self.mMomentumW = momentum*self.mMomentumW + (1-momentum)*tf.transpose(gradientW)
#        self.mMomentumSi = momentum*self.mMomentumSi + (1-momentum)*tf.transpose(gradientSi)
        self.mMomentumBi = momentum*self.mMomentumBi + (1-momentum)*tf.transpose(gradientBi)
        self.mMomentumBj = momentum*self.mMomentumBj + (1-momentum)*tf.transpose(gradientBj)
        self.mW += learning_rate*self.mMomentumW
        self.mBi += learning_rate*self.mMomentumBi
#        self.mSi += learning_rate*self.mMomentumSi
        self.mBj += learning_rate*self.mMomentumBj
    
    def train_step_grads_vars(self, data, learning_rate, momentum, CD_order):
        gradientW, gradientSi, gradientBi, gradientBj = self.CDk(data,CD_order)
        self.mMomentumW = momentum*self.mMomentumW + (1-momentum)*tf.transpose(gradientW)
#        self.mMomentumSi = momentum*self.mMomentumSi + (1-momentum)*tf.transpose(gradientSi)
        self.mMomentumBi = momentum*self.mMomentumBi + (1-momentum)*tf.transpose(gradientBi)
        self.mMomentumBj = momentum*self.mMomentumBj + (1-momentum)*tf.transpose(gradientBj)
        return [[-self.mMomentumW, self.mW],
                [-self.mMomentumBi, self.mBi],
                [-self.mMomentumBj, self.mBj]]
        
    def save(self,num_iter_all,session,name="RBM_save"):
        np.savez_compressed("./save/"+name+".npz",weights = [self.mW.eval(session = session)],
                            visSB = [self.mSi.eval(session = session), self.mBi.eval(session = session)],
                            hidB = [self.mBj.eval(session = session)],
                            size = [self.mNHid, self.mNVis],
                            nIter = num_iter_all)

        
#batch_size = 100        
#num_batch_per_epoch = 200
#num_epoch = 50
#myRBM = RBM(128,12)
#startingCDorder = 2
#
#num_iter_all = 10000

#
#data = dataset('./vehicle-trajectory-data/vehicle-trajectory-data-4/0400pm-0415pm/',
#               'trajectories-0400-0415.csv',
#               memoryLength = 5, horizon = 1)
#n_iter = 0
#try:
#    with tf.load("./save/RBM_save.npz") as reloadedvalues:
#        size = reloadedvalues["size"]
#        
#        assert(size[0]==myRBM.mNHid)
#        assert(size[1]==myRBM.mNVis)
#        myRBM.setWeights(reloadedvalues["weights"][0],
#                         reloadedvalues["visSB"][0],
#                         reloadedvalues["visSB"][1],
#                         reloadedvalues["hidB"][0])
#
#        n_iter = reloadedvalues["nIter"]
#        myRBM.setMomentums(reloadedvalues["weights"][1],
#                     reloadedvalues["visSB"][2],
#                     reloadedvalues["visSB"][3],
#                     reloadedvalues["hidB"][1])
#        print("Restored weights and momentums")
#       
#except:
#    n_iter = 0
#    
#while(n_iter < num_iter_all):
#    print("Creating new batch of data")
#    inputs_batch_epoch, targets_batch_epoch, neighborhood_batch_epoch = data.get_batch(batch_size*num_batch_per_epoch)
#    inputs_batch_epoch = (inputs_batch_epoch +10)/20
#    targets_batch_epoch = (targets_batch_epoch +10)/20
#    for i in range(num_epoch):
##        print("Restart an epoch")
#        shuffling = tf.random.permutation(inputs_batch_epoch.shape[0])
#        inputs_batch_epoch = inputs_batch_epoch[shuffling,:,:]
#        targets_batch_epoch = targets_batch_epoch[shuffling,:]
#        neighborhood_batch_epoch = neighborhood_batch_epoch[shuffling,:]
#        for j in range(num_batch_per_epoch):
#            n_iter +=1
#            if(n_iter > num_iter_all):
#                break
#            inputs_batch = inputs_batch_epoch[j*batch_size:(j+1)*batch_size,:,:]
#            targets_batch = targets_batch_epoch[j*batch_size:(j+1)*batch_size,:]
#            targets_batch = targets_batch[:,0:2]
#            
#            
#            inputs_batch = tf.reshape(inputs_batch,[-1,inputs_batch.shape[1]*inputs_batch.shape[2]])
#            visible = tf.float32(tf.concatenate((inputs_batch, targets_batch),axis=1))
#            myRBM.train_step(visible,0.1,0.9,1)#int(i/5)+startingCDorder)
#            if n_iter%1000==0:
#                print('iter %d' % n_iter)
#                
#myRBM.save(num_iter_all) 
#
#W = myRBM.mW   
#bi = myRBM.mBi   
#bj = myRBM.mBj   
#si = myRBM.mSi   
#
#
#index = tf.random.randint(0,50,1)
#x = myRBM.mW[index,even]
#y = myRBM.mW[index,odd]
#
#plt.plot(x,y)
#plt.plot(myRBM.mW[index,10],myRBM.mW[index,11],'+')
#
#plt.close()
#
#valueGenerated = myRBM.generate()
#
#plt.plot(valueGenerated[even],valueGenerated[odd])
#plt.plot(valueGenerated[10],valueGenerated[11],'+')
#
#plt.close()
#
#example_input, truth, neigh= data.get_example()
#example_input = (example_input[:,:,0:2]+10)/20
#truth = (truth+10)/20
#n = example_input.shape[0]
#m1 = example_input.shape[1]*example_input.shape[2]
#m2 = (example_input.shape[1]+1)*example_input.shape[2]
#reshaped_input = tf.zeros([n,m2])
#
#reshaped_input[:,0:m1] = tf.reshape(example_input,[n,m1])
#        
#prediction = myRBM.predict(reshaped_input, 1e-9)
#
#indexToPlot = 100
#plt.plot(prediction[:,0],prediction[:,1],'red',truth[:,0,0],truth[:,0,1],'green')
#plt.plot(prediction[indexToPlot,0],prediction[indexToPlot,1],'+',
#         truth[indexToPlot,0,0],truth[indexToPlot,0,1],'.',
#         example_input[indexToPlot,0,0],example_input[indexToPlot,0,1],'x',
#         example_input[indexToPlot,1,0],example_input[indexToPlot,1,1],'x',
#         example_input[indexToPlot,2,0],example_input[indexToPlot,2,1],'x',
#         example_input[indexToPlot,3,0],example_input[indexToPlot,3,1],'x',
#         example_input[indexToPlot,4,0],example_input[indexToPlot,4,1],'x',markersize=20)       
#
#
#tf.savez_compressed("./save/predictions.npz",pred = prediction,targets=truth,inputs=example_input)          

# Test to see if RBM is working on good data: it does work
#        
#def load():
#    with open("../data/mnist.pkl",'rb') as f:
#        mnist = pickle.load(f)
#    return mnist["training_images"], mnist["training_labels"], mnist["test_images"], mnist["test_labels"]
#
#inputs_placeholder = tf.placeholder(np.float32,[None,28*28])
#CD_order = tf.Variable(1)
#learning_rate = tf.Variable(0.005)
#momentum = tf.Variable(0.9)
#
#training, labels, test, test_labels = load()
#shuffling = np.random.permutation(training.shape[0])
#training = training[shuffling,:]
#labels = labels[shuffling]
#indexBatch = 0
#def getBatch(size):
#    global indexBatch
#    global training
#    global labels
#    batch = np.float32(training[indexBatch:indexBatch+size])/256
#    targets = labels[indexBatch:indexBatch+size]
#    indexBatch += size
#    if(indexBatch+size>training.shape[0]):
#        indexBatch = 0
#        shuffling = np.random.permutation(training.shape[0])
#        training = training[shuffling,:]
#        labels = labels[shuffling]
#    return batch,targets
#    
#mnistRBM = RBM(100,training.shape[1])
#
#sess = tf.Session()
#
#try:
#    with np.load("./save/RBM_save.npz") as reloadedvalues:
#        size = reloadedvalues["size"]
#        
##        assert(size[0]==mnistRBM.mNHid)
##        assert(size[1]==mnistRBM.mNVis)
#        mnistRBM.setWeights(reloadedvalues["weights"][0],
#                         reloadedvalues["visSB"][0],
#                         reloadedvalues["visSB"][1],
#                         reloadedvalues["hidB"][0])
#    
#        n_iter = reloadedvalues["nIter"]
##        mnistRBM.setMomentums(reloadedvalues["weights"][1],
##                     reloadedvalues["visSB"][2],
##                     reloadedvalues["visSB"][3],
##                     reloadedvalues["hidB"][1])
##        print("Restored weights and momentums")
#        sess.run(tf.global_variables_initializer())
#        print("Restored weights layer1")
#except:
#    print("Random initialization")
#    sess.run(tf.global_variables_initializer())
#    
##    
##optimizer = tf.train.GradientDescentOptimizer(learning_rate)
##tf_train_step = optimizer.apply_gradients(grads_and_vars=\
##    mnistRBM.train_step_grads_vars(inputs_placeholder,learning_rate,momentum,CD_order))
##for i in range(1000):
##    batch, targets = getBatch(500)
##    feed_dict = {inputs_placeholder : batch,
##                 learning_rate : 0.01,
##                 momentum : 0.5,
##                 CD_order : 1}
##    sess.run(tf_train_step,feed_dict=feed_dict)
##    if i%100 == 0:
##        print('iteration %d' % i)
##    for i in range(10000):
##        batch, targets = getBatch(100)
##        order = tf.abs(int(tf.random.randn(1)*3))+1
##        mnistRBM.train_step(batch,0.01,0.9,order)
##        if i%100 == 0:
##            print('iteration %d' % i)
##
#                        
##mnistRBM.save(1000,sess)
##
#W  = mnistRBM.mW.eval(session=sess)   
#bi = mnistRBM.mBi.eval(session=sess)   
#bj = mnistRBM.mBj.eval(session=sess)   
#si = mnistRBM.mSi.eval(session=sess)  
#    
#index = np.random.randint(0,10,1)    
#print(index)
#plt.imshow(np.reshape(W[index,:],[28,28]))
#ic = 2
#jc = 5
#f, axes = plt.subplots(nrows=ic, ncols=jc, figsize=(28, 28))
#for j in range(jc):
#    for i in range(ic):
#        axes[i,j].imshow(np.reshape(W[i+ic*j,:],[28,28]), cmap='gray')
#        axes[i,j].axis('off')
#
#mnistRBM2 = RBM(10,10)
#
#sess2 = tf.Session()
#try:
#    with tf.load("./save/RBM2_save.npz") as reloadedvalues:
#        size = reloadedvalues["size"]
#        
#        assert(size[0]==mnistRBM2.mNHid)
#        assert(size[1]==mnistRBM2.mNVis)
#        mnistRBM2.setWeights(reloadedvalues["weights"][0],
#                         reloadedvalues["visSB"][0],
#                         reloadedvalues["visSB"][1],
#                         reloadedvalues["hidB"][0])
#    
#        n_iter = reloadedvalues["nIter"]
##        mnistRBM2.setMomentums(reloadedvalues["weights"][1],
##                     reloadedvalues["visSB"][2],
##                     reloadedvalues["visSB"][3],
##                     reloadedvalues["hidB"][1])
#        print("Restored weights layer2")
#except:
#    hiddenBatch = mnistRBM.getHidden(inputs_placeholder,CD_order.eval(session=sess))
#    
#    optimizer = tf.train.GradientDescentOptimizer(learning_rate)
#    tf_train_step2 = optimizer.apply_gradients(grads_and_vars=\
#    mnistRBM2.train_step_grads_vars(hiddenBatch,learning_rate,momentum,CD_order))
#    sess2.run(tf.global_variables_initializer())
#    for i in range(100):
#        batch, targets = getBatch(100)
#        feed_dict = {inputs_placeholder : batch,
#                 learning_rate : 0.01,
#                 momentum : 0.5,
#                 CD_order : 1}
#        sess2.run(tf_train_step2,feed_dict=feed_dict)
#        
#        if i%100 == 0:
#            print('iteration %d' % i)
#    for i in range(50000):
#        batch, targets = getBatch(100)
#        feed_dict = {inputs_placeholder : batch,
#                 learning_rate : 0.005,
#                 momentum : 0.9,
#                 CD_order : 1}
#        sess2.run(tf_train_step2,feed_dict=feed_dict)
#        if i%100 == 0:
#            print('iteration %d' % i)
##
#mnistRBM2.save(10000,sess2,"./save/RBM2_save")
#
#W2 = mnistRBM2.mW.eval(session=sess2)
#bj2 = mnistRBM2.mBj.eval(session=sess2)
##index = np.random.randint(0,5,1)    
##plt.imshow(np.reshape(W2[index,:],[2,5]))
##ic = 1
##jc = 5
##f, axes = plt.subplots(nrows=jc, ncols=ic, figsize=(5, 10))
##for j in range(jc):
##    for i in range(ic):
##        axes[i,j].imshow(tf.reshape(W2[i+ic*j,:],[5,10]), cmap='gray')
##        axes[i,j].axis('off')
#
#
#
#myFC = ptfc()
#myFC.addPreTrainedLayer(np.transpose(W),bj)
#myFC.addPreTrainedLayer(np.transpose(W2),bj2)
#myFC.addLinearLayer(10)
#tf.get_default_graph().get_collection_ref(tf.GraphKeys.TRAINABLE_VARIABLES)
#
#targets_placeholder = tf.placeholder(np.uint8,[None])
#
#outputs = tf.nn.softmax(myFC.compute(inputs_placeholder))
#oneHotLabels = tf.one_hot(targets_placeholder,depth=10)
##oneHotLabels = tf.cast(tf.transpose(oneHotLabels,[0,2,1]),tf.int32)
#lossFC = tf.reduce_mean(tf.keras.losses.binary_crossentropy(oneHotLabels,outputs))
#optimizerLinear = tf.train.AdamOptimizer(0.001)
#linear_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,'linear')
#trainLinear = optimizerLinear.minimize(lossFC,var_list=linear_vars)
#optimizerFT = tf.train.AdamOptimizer(0.0001)
#fineTune = optimizerFT.minimize(lossFC)
#sess3 = tf.Session()
#sess3.run(tf.global_variables_initializer())
#for i in range(5000):
#    batch, targets = getBatch(500)
#    feed_dict = {inputs_placeholder : batch,
#                 targets_placeholder: targets}
#    sess3.run(trainLinear,feed_dict=feed_dict)
#    if i%1000==0:
#        print('iter %d' % i)
#for i in range(15000):
#    batch, targets = getBatch(500)
#    feed_dict = {inputs_placeholder : batch,
#                 targets_placeholder: targets}
#    sess3.run(fineTune,feed_dict=feed_dict)
#    if i%1000==0:
#        print('iter %d' % i)
#        lossFC_val = sess3.run(lossFC,feed_dict=feed_dict)
#        print('loss %f' % lossFC_val)
#    
#batch, targets = getBatch(1)
#feed_dict = {inputs_placeholder : batch}
#res = sess3.run(outputs,feed_dict=feed_dict)
#print('said %d actual %d confidence %f' %(np.argmax(res),targets, np.max(res)*100))
#print(res)
#plt.imshow(np.reshape(batch[:],[28,28]),cmap='Greys')
#
#W2 = np.transpose(myFC.mLayers[0].mW.eval(session=sess3))
#ic = 2
#jc = 5
#f, axes = plt.subplots(nrows=ic, ncols=jc, figsize=(28, 28))
#for j in range(jc):
#    for i in range(ic):
#        axes[i,j].imshow(np.reshape(W2[i+ic*j,:],[28,28]), cmap='gray')
#        axes[i,j].axis('off')
#        
#feed_dict = {inputs_placeholder : test,
#             targets_placeholder: test_labels}
#lossFC_val = sess3.run(lossFC,feed_dict=feed_dict)
#print(lossFC_val)
