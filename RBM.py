# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 10:23:43 2018

@author: a010943
"""

import tensorflow as tf
import numpy as np
from getBatch import dataset
import matplotlib.pyplot as plt
from urllib import request
import gzip
import pickle
import constantSpeedPredict as csp

even = np.array([0,2,4,6,8])
odd = np.array([1,3,5,7,9])

def sample_bernoulli(probabilities):
    random = np.random.uniform(low=0,high=1,size=probabilities.shape)
    return np.float32(np.less(random,probabilities))

def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))

class RBM:
    
    def __init__(self,n_hid, n_vis):
        self.mNHid = n_hid
        self.mNVis = n_vis
        self.mW = np.random.randn(n_hid,n_vis)*0.1
        self.mMomentum = np.zeros([n_hid,n_vis])
    
    def setWeights(self, weights):
        self.mNHid = weights.shape[0]
        self.mNVis = weights.shape[1]
        self.mW = weights
    
    def visible_state_to_hidden_probabilities(self, visible_state):
        deltaEnergy = np.matmul(visible_state,np.transpose(self.mW))
        return sigmoid(deltaEnergy)
    
    def hidden_state_to_visible_probabilities(self, hidden_state):
        deltaEnergy = np.matmul(hidden_state,self.mW)
        return sigmoid(deltaEnergy)
    

    def configuration_goodness(self, visible_state, hidden_state):
        return np.mean(
                np.sum(
                        np.multiply
                            (hidden_state,np.matmul(
                                    self.mW,np.transpose(visible_state)))
                            ,axis=0))
    
    def configuration_goodness_gradient(self,visible_state, hidden_state):
        
        return np.matmul(np.transpose(visible_state),hidden_state)/visible_state.shape[0]

    def cd1(self, visible_data):
        
        probaVH1 = self.visible_state_to_hidden_probabilities(visible_data)
        
        probaHV = self.hidden_state_to_visible_probabilities(probaVH1)
        
        probaVH2 = self.visible_state_to_hidden_probabilities(probaHV)
        
        positive = self.configuration_goodness_gradient(visible_data,probaVH1)
        negative = self.configuration_goodness_gradient(probaHV,probaVH2)
        
        return positive-negative
    
    def predict(self, input_data, eps):
        
        pred_temp =  (input_data[:,-2:]).copy()
        data_temp = np.zeros([input_data.shape[0],5,2])
        data_temp[:,:,0] = input_data[:,even]
        data_temp[:,:,1] = input_data[:,odd]
        input_data[:,-2:] = csp.constantSpeed(data_temp,10)
        
#        while(np.mean(np.square(pred_temp-input_data[:,-2:]))>eps):  
        for i in range(1000):
            pred_temp=(input_data[:,-2:]).copy()
            probaVH = self.visible_state_to_hidden_probabilities(input_data) # get hidden state
            probaHV = self.hidden_state_to_visible_probabilities(probaVH) # get visible state
            input_data[:,-2:] = probaHV[:,-2:]                            # update only prediction state
#            print('loss %1.10f' % np.mean(np.fabs(pred_temp-probaHV[:,-2:])))

        return input_data[:,-2:]
        
    
    def train_step(self, data, learning_rate, momentum):
        gradient = self.cd1(data)
        self.mMomentum = 0.9*self.mMomentum + np.transpose(gradient)
        self.mW += learning_rate*self.mMomentum
        
    def save(self,num_iter_all,name="RBM_save"):
        np.savez_compressed(name+".npz",weights = self.mW,size = [self.mNHid, self.mNVis], nIter = num_iter_all)

        
batch_size = 100        
num_batch_per_epoch = 200
num_epoch = 50
myRBM = RBM(256,12)

num_iter_all = 100000


#data = dataset('./vehicle-trajectory-data/vehicle-trajectory-data-4/0400pm-0415pm/',
#               'trajectories-0400-0415.csv',
#               memoryLength = 5, horizon = 1)
n_iter = 0
try:
    with np.load("./RBM_save.npz") as reloadedvalues:
        myRBM.setWeights(reloadedvalues["weights"])
        size = reloadedvalues["size"]
        assert(size[0]==myRBM.mNHid)
        assert(size[1]==myRBM.mNVis)
        n_iter = reloadedvalues["nIter"]
        print("Restored weights")
except:
    n_iter = 0
    
while(n_iter < num_iter_all):
    print("Creating new batch of data")
    inputs_batch_epoch, targets_batch_epoch, neighborhood_batch_epoch = data.get_batch(batch_size*num_batch_per_epoch)
    inputs_batch_epoch = (inputs_batch_epoch + 10)/20 
    targets_batch_epoch = (targets_batch_epoch + 10)/20
    for i in range(num_epoch):
#        print("Restart an epoch")
        shuffling = np.random.permutation(inputs_batch_epoch.shape[0])
        inputs_batch_epoch = inputs_batch_epoch[shuffling,:,:]
        targets_batch_epoch = targets_batch_epoch[shuffling,:]
        neighborhood_batch_epoch = neighborhood_batch_epoch[shuffling,:]
        for j in range(num_batch_per_epoch):
            n_iter +=1
            if(n_iter > num_iter_all):
                break
            inputs_batch = inputs_batch_epoch[j*batch_size:(j+1)*batch_size,:,:]
            targets_batch = targets_batch_epoch[j*batch_size:(j+1)*batch_size,:]
            targets_batch = targets_batch[:,0:2]
            
            
            inputs_batch = np.reshape(inputs_batch,[-1,inputs_batch.shape[1]*inputs_batch.shape[2]])
            visible = np.float32(np.concatenate((inputs_batch, targets_batch),axis=1))
            myRBM.train_step(visible, 0.09,0.9)
            if n_iter%1000==0:
                print('iter %d' % n_iter)
                
myRBM.save(num_iter_all)    
index = np.random.randint(0,50,1)


x = myRBM.mW[index,even]
y = myRBM.mW[index,odd]

plt.plot(x,y)
plt.plot(myRBM.mW[index,10],myRBM.mW[index,11],'+')

plt.close()


example_input, truth, neigh= data.get_example()
example_input = (example_input[:,:,0:2]+10)/20
truth = (truth+10)/20
n = example_input.shape[0]
m1 = example_input.shape[1]*example_input.shape[2]
m2 = (example_input.shape[1]+1)*example_input.shape[2]
reshaped_input = np.zeros([n,m2])

reshaped_input[:,0:m1] = np.reshape(example_input,[n,m1])
        
prediction = myRBM.predict(reshaped_input, 1e-9)

indexToPlot = 100
plt.plot(prediction[:,0],prediction[:,1],'red',truth[:,0,0],truth[:,0,1],'green')
plt.plot(prediction[indexToPlot,0],prediction[indexToPlot,1],'+',
         truth[indexToPlot,0,0],truth[indexToPlot,0,1],'.',
         example_input[indexToPlot,0,0],example_input[indexToPlot,0,1],'x',
         example_input[indexToPlot,1,0],example_input[indexToPlot,1,1],'x',
         example_input[indexToPlot,2,0],example_input[indexToPlot,2,1],'x',
         example_input[indexToPlot,3,0],example_input[indexToPlot,3,1],'x',
         example_input[indexToPlot,4,0],example_input[indexToPlot,4,1],'x',markersize=20)       
        

# Test to see if RBM is working on good data: it does work
        
#def load():
#    with open("mnist.pkl",'rb') as f:
#        mnist = pickle.load(f)
#    return mnist["training_images"], mnist["training_labels"], mnist["test_images"], mnist["test_labels"]
#
#
#training, labels, test, test_labels = load()
#
#index = 0
#def getBatch(size):
#    global index
#    batch = np.float32(training[index:index+size])/256
#    index = np.mod(index+size,training.shape[0])
#    return batch
#    
#mnistRBM = RBM(100,training.shape[1],100)
#for i in range(5000):
#    batch = getBatch(100)
#    mnistRBM.train_step(batch,0.09,0.9)
#    
#index = np.random.randint(0,100,1)    
#print(index)
#plt.imshow(np.reshape(mnistRBM.mW[index,:],[28,28]))
#test = mnistRBM.mW
